#include <iostream>
#include <string>
#include <cmath>
using namespace std;

typedef struct node {
  char key;
  node *l, *r;
} node;

node * create(char key) {
  node * root = (node *) malloc(sizeof(node));
  root->key = key;
  root->l = root->r = NULL;
  return root;
}

node * create_tree(const char * input, int * cur) {
  node * root = NULL;
  if (input[*cur] == '(') {
    (*cur)++;
    root = create(input[*cur]);
    (*cur)++;
    root->l = create_tree(input, cur);
    root->r = create_tree(input, cur);
    (*cur)++;
  }
  return root;
}

void preorder(node * root) {
  if (root != NULL) {
    cout << root->key;
    preorder(root->l);
    preorder(root->r);
  }
}

int height(node * root) {
  if (root == NULL) return -1;
  return height_t(root->l) > height_t(root->r) ? height_t(root->l) + 1 : height_t(root->r) + 1;
}



void print_tree(node * root, int height, int h) {
  if (root == NULL) {
    for (int i = 0; i < pow(2, h + 1) - 1; i++)
      cout << ' ';
    return;
  }
  if (height == 0) {
    for (int i = 0; i<(pow(2,h)-1); i++)
      cout << ' ';
    cout << root->key;
    for (int i = 0; i<pow(2,h); i++)
      cout << ' ';
  }
  else if (height > 0) {
    print_tree(root->l, height - 1, h);
    print_tree(root->r, height - 1, h);
  }
}

void print_wrap(node * root) {
  h = height(root);
  for (int i = 0; i <= h; i++) {
    print_tree(root, i, h - i);
    cout << '\n';
  }
}

int main() {
  int i = 0;
  node * tree = create_tree("(F(A(C(B)(T)))(D(G)))", &i);
  //preorder(tree);
  //cout << " " << height(tree);
  print_wrap(tree);

  return 0;
}
